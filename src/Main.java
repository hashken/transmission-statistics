import settings.Settings;
import transmission.worker.MonitorThread;
import transmission.worker.ObserverThread;
import transmission.worker.interfaces.Observer;
import utils.Print;

import java.io.IOException;

public class Main
{
	public static void main(String[] args)
	{
		
		try 
		{
			Settings      s     = new Settings();
			s.loadSettings();
	        //create subject
	        MonitorThread topic = new MonitorThread(s);
	        //create observers
	        Observer      obj1  = new ObserverThread();
	        //register observers to the subject
	        topic.register(obj1);
	        //attach observer to subject
	        obj1.setSubject(topic);
	        //now send message to subject
	        (new Thread(topic)).start();
		} 
		catch (IOException e) 
		{
			Print.error(Main.class.getClass(), "Error loading settings.");
			e.printStackTrace();
		}
		while(true)
		{
			
		}

	}
}
