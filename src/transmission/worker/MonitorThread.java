package transmission.worker;


import org.joda.time.DateTime;
import settings.Settings;
import transmission.TransmissionAdapter;
import transmission.response.POJOs.torrentlist.Torrent;
import transmission.storage.SqLiteStorageRepo;
import transmission.storage.interfaces.TorrentStorage;
import transmission.worker.interfaces.Observer;
import transmission.worker.interfaces.Subject;
import utils.Print;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MonitorThread implements Subject, Runnable {

    // Transmission Communication
    private TransmissionAdapter adapter;
    private TorrentStorage      repo;

    // Observers
    private List<Observer>      observers;
    private DataContainer       data;
    private Settings            settings;
    private boolean             changed;
    private final               Object     MUTEX = new Object();

    public MonitorThread(Settings settings)
    {
        // Initialize the list of observers.
        this.observers = new ArrayList<Observer>();
        // Take out the updateinterval
        this.adapter   = new TransmissionAdapter(settings);
        this.repo      = SqLiteStorageRepo.getInstance();
        this.settings  = settings;
        this.data      = new DataContainer();
    }
    //------------------------------------------------------------------------//
    //---- OBSERVER PATTERN CODE ---------------------------------------------//
    //------------------------------------------------------------------------//

	@Override
	public void register(Observer obj)
	{
		if (obj == null)
			throw new NullPointerException("Null Observer");
		synchronized (MUTEX)
		{
			if (!observers.contains(obj))
				observers.add(obj);
		}
	}
 
	@Override
	public void unregister(Observer obj)
	{
		synchronized (MUTEX)
		{
			observers.remove(obj);
		}
	}
    /*
     * Use hash of the data to check for change instead of a copy.
     * Enabled single read (keep hash and set variable to null (mem))
     * Compare hash for change.
     */
	@Override
	public void notifyObservers() {
		List<Observer> observersLocal = null;
		// synchronization is used to make sure any observer registered after
		// message is received is not notified
		synchronized (MUTEX) {
			if (!changed)
				return;
			// Would it be better to pic either list or arraylist but not
			// convert? (performance)
			observersLocal = new ArrayList<Observer>(this.observers);
			this.changed = false;
		}
		for (Observer obj : observersLocal) {
			obj.update();
		}
	}


    @Override
    public Object getUpdate(Observer obj) {
        return this.data;
    }
     
    //method to post message to the topic
    public void updateData(DataContainer data){
    	Print.debugMessage(this.getClass(), "Updating data");
        this.data    = data;
        this.changed = true;
        notifyObservers();
    }
    //------------------------------------------------------------------------//
    //---- THREAD CODE -------------------------------------------------------//
    //------------------------------------------------------------------------//
	@Override
	public void run() {
		while(true)
		{
			//Fetch data periodically.
			try {
				DataContainer snapshot = getData();
				snapshot.setNextUpdate(new DateTime().plusMillis(settings.getUpdateInterval() * 60000));
				updateData(snapshot);
				Thread.sleep(settings.getUpdateInterval() * 60000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
    //------------------------------------------------------------------------//
    //---- TRANSMISSION COMMUNICATION ----------------------------------------//
    //------------------------------------------------------------------------//
	private DataContainer getData()
	{
		Long    mostUpDaily   = Long.MIN_VALUE,  
				mostUpWeekly  = Long.MIN_VALUE;
		Float   maxRatio      = 0.0f;
		Torrent mostUpDailyT  = null,
				mostUpWeeklyT = null,
				maxRatioT     = null;
		try
		{
			// Get the list of active torrents
			List<Torrent> torrents = adapter.GetTorrentList();
			for(Torrent t : torrents)
			{
                // /!\ This must be done first! /!\
                // Get the details for this torrent.
                adapter.UpdateTorrentDetails(t);

                // If we are tracking this torrent long enough,
                // it has been in transmission long enough
                // and it has not uploaded enough we delete it.
                DateTime trackedSince      = repo.trackedDays(t);
                DateTime minimumTracking   = new DateTime().minusDays(settings.getDeleteThreshold());
                Long     megabytesUploaded = repo.uploadedLastNDays(t, 14) / 1000000;

                if(trackedSince != null                                 && // Null if not tracked at all.
                   trackedSince.isBefore(minimumTracking)               &&
                   t.getAgeInDays() >= settings.getDeleteAgeThreshold() &&
                   megabytesUploaded <= settings.getDeleteThreshold()     ) // From megabyte to bytes.
                {
                    adapter.deleteTorrent(t);
                    data.addDeletedTorrent(t);
                }

				// Put it in the database.
				repo.storeSnapShot(t);
				// Get uploaded this week, uploaded today and ratio.
				Long uploadedToday  = repo.uploadedToday(t);
				Long uploadedWeekly = repo.uploadedWeekly(t);
				
				// Determine highest values.
				if(uploadedToday > mostUpDaily)
				{
					mostUpDaily  = uploadedToday;
					mostUpDailyT = t;
				}
				if(uploadedWeekly > mostUpWeekly)
				{
					mostUpWeekly  = uploadedWeekly;
					mostUpWeeklyT = t;
				}
				if(t.getUploadRatio() > maxRatio)
				{
					maxRatio  = t.getUploadRatio();
					maxRatioT = t;
				}
			}
			// Insert values in DataContainer
			data.setMaxUploadThisWeek(mostUpWeekly, mostUpWeeklyT);
			data.setMaxUploadToday(mostUpDaily, mostUpDailyT);
			data.setMaxRatio(maxRatio, maxRatioT);
			return data;
			
		} catch (IOException e)
		{
			Print.error(this.getClass(), "Error retrieving torrents from Transmission!");
		}
		return data;
	}
}
