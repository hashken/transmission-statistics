package transmission.worker;

import transmission.response.POJOs.torrentlist.Torrent;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

public class DataContainer
{

    private Long          mostUpWeekly;
    private Torrent       mostUpWeeklyT;
    private Long          mostUpDaily;
    private Torrent       mostUpDailyT;
    private Float         maxRatio;
    private Torrent       maxRatioT;
	private DateTime	  nextUpdate;

    private List<Torrent> deletedTorrents;


    public void setMaxUploadThisWeek(Long mostUpWeekly, Torrent mostUpWeeklyT)
    {
        this.mostUpWeekly = mostUpWeekly;
        this.mostUpWeeklyT = mostUpWeeklyT;
    }

    public void setMaxUploadToday(Long mostUpDaily, Torrent mostUpDailyT)
    {
        this.mostUpDaily = mostUpDaily;
        this.mostUpDailyT = mostUpDailyT;
    }

    public void setMaxRatio(Float ratio, Torrent maxRatioT)
    {
        this.maxRatio = ratio;
        this.maxRatioT = maxRatioT;
    }

    //-------------------------------------------------------------------------/
    //---- GETTERS AND SETTERS ------------------------------------------------/
	//-------------------------------------------------------------------------/
	public Long getMostUpWeekly()
	{
		return mostUpWeekly;
	}

	public void setMostUpWeekly(Long mostUpWeekly)
	{
		this.mostUpWeekly = mostUpWeekly;
	}

	public Torrent getMostUpWeeklyT()
	{
		return mostUpWeeklyT;
	}

	public void setMostUpWeeklyT(Torrent mostUpWeeklyT)
	{
		this.mostUpWeeklyT = mostUpWeeklyT;
	}

	public Long getMostUpDaily()
	{
		return mostUpDaily;
	}

	public void setMostUpDaily(Long mostUpDaily)
	{
		this.mostUpDaily = mostUpDaily;
	}

	public Torrent getMostUpDailyT()
	{
		return mostUpDailyT;
	}

	public void setMostUpDailyT(Torrent mostUpDailyT)
	{
		this.mostUpDailyT = mostUpDailyT;
	}

	public Float getMaxRatio()
	{
		return maxRatio;
	}

	public void setMaxRatio(Float maxRatio)
	{
		this.maxRatio = maxRatio;
	}

	public Torrent getMaxRatioT()
	{
		return maxRatioT;
	}

	public void setMaxRatioT(Torrent maxRatioT)
	{
		this.maxRatioT = maxRatioT;
	}

    public List<Torrent> getDeletedTorrents()
    {
        return deletedTorrents;
    }

    public void setDeletedTorrents(List<Torrent> deletedTorrents)
    {
        this.deletedTorrents = deletedTorrents;
    }

    public void addDeletedTorrent(Torrent t)
    {
        if(this.deletedTorrents == null) this.deletedTorrents = new LinkedList<Torrent>();
        this.deletedTorrents.add(t);
    }

	public void setNextUpdate(DateTime i)
	{
		this.nextUpdate = i;
		
	}
	public DateTime getNextUpdate()
	{
		return this.nextUpdate;
	}

}
