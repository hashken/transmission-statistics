package transmission.worker;

import transmission.response.POJOs.torrentlist.Torrent;
import transmission.worker.interfaces.Observer;
import transmission.worker.interfaces.Subject;
import utils.Print;
import utils.StringUtils;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class ObserverThread implements Observer
{
    private Subject topic;

    public ObserverThread()
    {
    }

    @Override
    public void update()
    {
        DataContainer data = (DataContainer) topic.getUpdate(this);
        if (data == null)
        {
            Print.debugMessage(this.getClass(), "No new data received!");
		} else
			printData(data);
	}
    
	private void printData(DataContainer data)
	{

        final String columnheader = "|------------------------------------------------------------------------------|";

        // Build the output table.
		StringBuilder sb = new StringBuilder();
        sb.append(columnheader + "\n");
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss");
		sb.append(String.format("| Snapshot: %66s |\n",  dtf.print(new DateTime())));
		sb.append(String.format("| Next Update: %63s |\n", dtf.print(data.getNextUpdate())));
        sb.append(columnheader + "\n");

		sb.append(String.format("| %-18s || %7s  || %42s |\n",
                "Maximum Ratio",
                data.getMaxRatio(),
                StringUtils.truncate(data.getMaxRatioT().getName(), 42)));
		sb.append(String.format("| %-18s || %5s MB || %42s |\n",
                "Max Upload Daily" ,
                data.getMostUpDaily()  / 1000000 ,
                StringUtils.truncate(data.getMostUpDailyT().getName(), 42)));
		sb.append(String.format("| %-18s || %5s MB || %42s |\n",
                "Max Upload Weekly",
                data.getMostUpWeekly() / 1000000,
                StringUtils.truncate(data.getMostUpWeeklyT().getName(), 42)));
        sb.append(columnheader + "\n");
        sb.append("| Deleted torrents                                                             |\n");
        sb.append(columnheader + "\n");
        if(data.getDeletedTorrents() != null)
            for(Torrent t : data.getDeletedTorrents())
                sb.append(String.format("| %76s |\n", t.getName()));
            else
                sb.append("| None                                                                         |\n");
        sb.append(columnheader + "\n");

        // Clear the console.
        Print.clearConsole();
        // Print the table.
		System.out.println(sb.toString());
	}
    @Override
    public void setSubject(Subject sub) {
        this.topic=sub;
    }
}
