package transmission.storage;

import org.joda.time.DateTime;
import transmission.response.POJOs.torrentlist.Torrent;
import transmission.storage.interfaces.TorrentStorage;
import utils.Print;

import java.sql.*;

public class SqLiteStorageRepo implements TorrentStorage
{
	// Singleton instance.
	private static TorrentStorage	instance	= null;
	private Connection				c;

	protected SqLiteStorageRepo(){}

	public static TorrentStorage getInstance()
	{
		if (instance == null)
			instance = new SqLiteStorageRepo();
		return instance;
	}

	// -------------------------------------------------------------------------/
	// ---- INTERFACE METHODS
	// --------------------------------------------------/
	// -------------------------------------------------------------------------/
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * transmission.storage.interfaces.TorrentStorage#storeSnapShot(transmission
	 * .response.POJOs.torrentlist.Torrent)
	 */
	@Override
	public void storeSnapShot(Torrent t)
	{

		// Pick out data we want to store for a single torrent snapshot.
		Long   totalUpload   = t.getBytesUploadedEver();
		Long   totalDownload = t.getBytesDownloadedEver();
		String name          = t.getName();
		String magnet        = t.getMagnetLink();
		Long   creationDate  = t.getDateCreated();

		// Check to see if we have details about it.
		// If not abort.
		if (totalUpload == null || totalDownload == null)
		{
			Print.error(this.getClass(),
					"No details fetched for this torrent! (" + name + ")");
			return;
		}

		// Insert or ignore torrent.
		String statement = String.format(
				"INSERT OR IGNORE INTO tblTorrents (name, magnetlink, dateCreated) "
						+ "VALUES ('%s', '%s', %s);", name, magnet,
				creationDate);
		executeUpdateQuery(statement);

		// Insert snapshot.
		statement = String
				.format("INSERT INTO tblSnapshots (date, downloaded, uploaded, torrent_name, torrent_magnet)"
						+ "VALUES (strftime('%%s', 'now'), '%s', '%s', '%s', '%s');",
						totalDownload, totalUpload, name, magnet);
		executeUpdateQuery(statement);
	}

	public Long uploadedToday(Torrent t)
	{
		Long now       = new DateTime().getMillis() / 1000;
		Long beginning = new DateTime().withTimeAtStartOfDay().getMillis() / 1000;

		// Get the ID for the torrent if it exists.
		String selectStatement = String.format("SELECT * FROM tblSnapshots \n"
				+ "WHERE torrent_name=\"%s\"\n"
				+ " AND torrent_magnet=\"%s\"\n" + " AND date <= %s\n"
				+ " AND date >= %s;", t.getName(), t.getMagnetLink(), now,
				beginning);
		ResultSet result = executeReadQuery(selectStatement);

		// Iterate over the resultset and sum the values.
		try
		{
			Long totalUpload   = 0L;
			Long minimumUpload = Long.MAX_VALUE;
			Long maximumUpload = 0L;
			while (result.next())
			{
				Long uploadThen = Long.parseLong(result.getString("uploaded"));
				if (uploadThen < minimumUpload)
					minimumUpload = uploadThen;
				
				if (uploadThen > maximumUpload)
					maximumUpload = uploadThen;
			}
			totalUpload = maximumUpload - minimumUpload;
			return totalUpload;
		} catch (SQLException e)
		{
			Print.error(this.getClass(),
					"Error creating sum of upload for today!");
			e.printStackTrace();
		}
		return null;

	}

	public Long uploadedWeekly(Torrent t)
	{
		Long now       = new DateTime().getMillis() / 1000;
		Long beginning = new DateTime().withTimeAtStartOfDay()
				                       .minusDays(7)
				                       .getMillis() / 1000;

		// Get the ID for the torrent if it exists.
		String selectStatement = String.format("SELECT * FROM tblSnapshots \n"
				+ "WHERE torrent_name=\"%s\"\n"
				+ " AND torrent_magnet=\"%s\"\n" + " AND date <= %s\n"
				+ " AND date >= %s;", t.getName(), t.getMagnetLink(), now,
				beginning);
		ResultSet result = executeReadQuery(selectStatement);

		// Iterate over the resultset and sum the values.
		try
		{
			Long totalUpload   = 0L;
			Long minimumUpload = Long.MAX_VALUE;
			Long maximumUpload = 0L;
			while (result.next())
			{
				Long uploadThen = Long.parseLong(result.getString("uploaded"));
				if (uploadThen < minimumUpload)
					minimumUpload = uploadThen;
				if (uploadThen > maximumUpload)
					maximumUpload = uploadThen;
			}
			totalUpload = maximumUpload - minimumUpload;
			return totalUpload;
		} catch (SQLException e)
		{
			Print.error(this.getClass(),
					"Error creating sum of upload for week!");
			e.printStackTrace();
		}
		return null;

	}

	public Long uploadedLastNDays(Torrent t, int numberOfDays)
	{
		Long now       = new DateTime().getMillis() / 1000;
		Long beginning = new DateTime().withTimeAtStartOfDay()
				                       .minusDays(numberOfDays)
				                       .getMillis() / 1000;

		// Get the ID for the torrent if it exists.
		String selectStatement = String.format("SELECT * FROM tblSnapshots \n"
				+ "WHERE torrent_name=\"%s\"\n"
				+ " AND torrent_magnet=\"%s\"\n" + " AND date <= %s\n"
				+ " AND date >= %s;", t.getName(), t.getMagnetLink(), now,
				beginning);
		ResultSet result = executeReadQuery(selectStatement);

		// Iterate over the resultset and sum the values.
		try
		{
			Long totalUpload   = 0L;
			Long minimumUpload = Long.MAX_VALUE;
			Long maximumUpload = 0L;
			while (result.next())
			{
				Long uploadThen = Long.parseLong(result.getString("uploaded"));
				if (uploadThen < minimumUpload)
					minimumUpload = uploadThen;
				if (uploadThen > maximumUpload)
					maximumUpload = uploadThen;
			}
			totalUpload = maximumUpload - minimumUpload;
			return totalUpload;
		} catch (SQLException e)
		{
			Print.error(this.getClass(),
					"Error creating sum of upload for week!");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public DateTime trackedDays(Torrent t)
	{
		// Get the ID for the torrent if it exists.
		// String selectStatement =
		// String.format("SELECT MIN(date) FROM tblSnapshots WHERE torrent_name=\"%s\";",
		// t.getName());
		String selectStatement = String
				.format("SELECT MIN(date) FROM tblSnapshots WHERE torrent_name=\"%s\";",
						t.getName());
		ResultSet result = executeReadQuery(selectStatement);

		try
		{
			if (!result.next())
				return null;
			while (result.next())
			{
				long     ageEpoch     = Integer.parseInt(result.getString(1));
				DateTime trackedSince = new DateTime(ageEpoch * 1000);
				return trackedSince;
			}
		} catch (SQLException e)
		{
			Print.error(this.getClass(),
					"Error determining tracking age of torrent.");
			e.printStackTrace();
		}
		return null;

	}

	// -------------------------------------------------------------------------/
	// ---- LOGIC METHODS
	// ------------------------------------------------------/
	// -------------------------------------------------------------------------/
	/**
	 * Simply connects to the database.
	 */
	private void connect()
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:statistics.db");
			c.setAutoCommit(false);

			// Check to see if the necessary tables exist, if not create them.
			if (!tableExists("tblTorrents"))
				createTorrentsTable();
			if (!tableExists("tblSnapshots"))
				createSnapShotsTable();
		} catch (Exception e)
		{
			Print.error(this.getClass(), e.getMessage());
			System.exit(0);
		}
		Print.debugMessage(this.getClass(), "Opened database successfully");
	}

	/**
	 * Executes an update query.
	 * 
	 * @param query
	 */
	private void executeUpdateQuery(String query)
	{
		// Connect to the database.
		if (c == null)
			connect();

		// Create a statement.
		Statement stmt;
		try
		{
			stmt             = c.createStatement();
			int  returnValue = stmt.executeUpdate(query);
			stmt.close();
			c.commit();
			Print.debugMessage(this.getClass(), "(" + returnValue + ")"
					+    "Executed   with success: " + query);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Executes a creation query for tables.
	 * 
	 * @param query
	 */
	private void executeCreateQuery(String query)
	{
		// Connect to the database.
		if (c == null)
			connect();

		// Create a statement.
		Statement stmt;
		try
		{
			stmt = c.createStatement();
			int returnValue = stmt.executeUpdate(query);
			stmt.close();
			Print.debugMessage(this.getClass(), "(" + returnValue + ")"
					+ "Executed with success: " + query);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Executes a read query and returns a resultset.
	 * 
	 * @param query
	 * @return
	 */
	private ResultSet executeReadQuery(String query)
	{
		// Connect to the database.
		if (c == null)
			connect();

		// Create a statement.
		Statement stmt;
		try
		{
			stmt = c.createStatement();
			ResultSet returnValue = stmt.executeQuery(query);
			Print.debugMessage(this.getClass(), String.format(
					"(%s) Executed with success: %s", returnValue, query));

			return returnValue;
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	// -------------------------------------------------------------------------/
	// ---- TABLE CREATION METHODS
	// ---------------------------------------------/
	// -------------------------------------------------------------------------/
	/**
	 * Returns a boolean value indicating wether or not a certain table already
	 * exists.
	 * 
	 * @param tableName
	 * @return
	 */
	private boolean tableExists(String tableName)
	{
		String tableTest = String
				.format("SELECT name FROM sqlite_master WHERE type='table' AND name='%s';",
						tableName);
		ResultSet result = executeReadQuery(tableTest);

		try
		{
			boolean empty = result.next();
			return empty;
		} catch (SQLException e)
		{
			return false;
		}
	}

	/**
	 * Creates the table for torrents.
	 */
	private void createTorrentsTable()
	{
		String createStatement = "CREATE TABLE \"main\".\"tblTorrents\" (\n"
				+ "    \"name\" TEXT NOT NULL,\n"
				+ "    \"magnetlink\" TEXT NOT NULL,\n"
				+ "    \"dateCreated\" TEXT NOT NULL, \n"
				+ "    PRIMARY KEY(\"name\", \"magnetlink\")\n" + ");";

		executeCreateQuery(createStatement);
	}

	/**
	 * Creates a table for snapshots. Torrent 1->* Snapshots.
	 */
	private void createSnapShotsTable()
	{
		String createStatement = "CREATE TABLE  tblSnapshots (\n"
				+ "	id INTEGER UNIQUE NOT NULL PRIMARY KEY,\n"
				+ "	downloaded INTEGER,\n"
				+ "	uploaded INTEGER,\n"
				+ "  date INTEGER, \n"
				+ "	torrent_name TEXT,\n"
				+ "	torrent_magnet TEXT,\n"
				+ "	FOREIGN KEY (torrent_name) REFERENCES tblTorrents(name) ON DELETE CASCADE,\n"
				+ "	FOREIGN KEY (torrent_magnet) REFERENCES tblTorrents(magnetlink) ON DELETE CASCADE\n"
				+ ");";
		executeCreateQuery(createStatement);
	}

}