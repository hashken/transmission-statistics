package transmission.storage.interfaces;


import org.joda.time.DateTime;
import transmission.response.POJOs.torrentlist.Torrent;

public interface TorrentStorage
{
	/**
	 * Creates a snapshot of this torrent in the SQL database.
	 * It stores total upload and download for the current time.
	 * @param t
	 */
	public abstract void storeSnapShot(Torrent t);
	
	/**
	 * Returns a value in bytes indicating how many bytes this
	 * torrent has uploaded the last 24 hours.
	 * @param t
	 * @return
	 */
	public abstract Long uploadedToday(Torrent t);
	
	public abstract Long uploadedWeekly(Torrent t);
    /**
     * Returns the bytes uploaded daysAgo back.
     * @param daysAgo Number of days to go back.
     * @param t torrent for which we need the data.
     * @return
     */
	public abstract Long uploadedLastNDays(Torrent t, int daysAgo);

    /**
     * Returns the number of days this torrent has been tracked
     * by the application.
     * @param t
     * @return
     */
    public abstract DateTime trackedDays(Torrent t);
	


}