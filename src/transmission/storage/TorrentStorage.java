package transmission.storage;


import transmission.response.POJOs.torrentlist.Torrent;

public interface TorrentStorage
{
	/**
	 * Creates a snapshot of this torrent in the SQL database.
	 * It stores total upload and download for the current time.
	 * @param t
	 */
	public abstract void storeSnapShot(Torrent t);
	
	/**
	 * Returns a value in bytes indicating how many bytes this
	 * torrent has uploaded the last 24 hours.
	 * @param t
	 * @return
	 */
	public abstract Long uploadedToday(Torrent t);
	
	public abstract Long uploadedWeekly(Torrent t);
	
	public abstract Long uploadedLastNDays(Torrent t, int daysAgo);
	
	/**
	 * Returns the bytes uploaded between two unix epoch time values.
	 * @param begin epoch time ticks to create interval.
	 * @param end
	 * @param t torrent for which we need the data.
	 * @return
	 */
	//public abstract Long uploadedBetween(Long begin, Long end, Torrent t);
	

}