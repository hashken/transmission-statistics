package transmission.statistics;

import org.apache.http.client.ClientProtocolException;
import transmission.TransmissionAdapter;
import transmission.response.POJOs.torrentlist.Torrent;
import transmission.storage.interfaces.TorrentStorage;

import java.io.IOException;
import java.util.List;

public class Stats
{
	private static TorrentStorage      repo     = null;
	private static Stats               instance = null;
	private static TransmissionAdapter adapter  = null;

	protected Stats()
	{
	}

	public static Stats getInstance(TorrentStorage repo,
			TransmissionAdapter adapter)
	{
		if (instance == null)
		{
			instance      = new Stats();
			Stats.repo    = repo;
			Stats.adapter = adapter;
		}
		return instance;
	}

	/**
	 * Iterates over the data in the database to determine the maximum upload
	 * for this day.
	 * 
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Torrent maximumUploadToday() throws ClientProtocolException,
			IOException
	{
		List<Torrent> list           = adapter.GetTorrentList();
		Long          maxUploadToday = Long.MIN_VALUE;
		Torrent       bestToday      = null;

		for (Torrent t : list)
		{
			// Determine best today.
			Long forToday = repo.uploadedToday(t);
			if (forToday > maxUploadToday)
			{
				maxUploadToday = forToday;
				bestToday      = t;
			}
		}
		bestToday.setDailyUpload(maxUploadToday);
		return bestToday;
	}

	public Torrent maximumRatio() throws ClientProtocolException, IOException
	{
		// Loop over torrents and update values in database.
		List<Torrent> list            = adapter.GetTorrentList();
		double        maxRatio        = Double.MIN_VALUE;
		Torrent       maxRatioTorrent = null;

		for (Torrent t : list)
		{
			adapter.UpdateTorrentDetails(t);
			repo.storeSnapShot(t);
			// Determine best ratio.
			if (t.getUploadRatio() > maxRatio)
			{
				maxRatio        = t.getUploadRatio();
				maxRatioTorrent = t;
			}
		}
		return maxRatioTorrent;
	}

	public Torrent maximumUploadedEver() throws ClientProtocolException,
			IOException
	{
		// Loop over torrents and update values in database.
		List<Torrent> list             = adapter.GetTorrentList();
		double        maxUpload        = Double.MIN_VALUE;
		Torrent       maxUploadTorrent = null;
		for (Torrent t : list)
		{
			adapter.UpdateTorrentDetails(t);
			repo.storeSnapShot(t);

			// Determine best uploaded ever.
			if (t.getBytesUploadedEver() > maxUpload)
			{
				maxUpload        = t.getBytesUploadedEver();
				maxUploadTorrent = t;
			}
		}
		return maxUploadTorrent;
	}

	public Torrent maximumUploadWeekly() throws ClientProtocolException,
			IOException
	{
		List<Torrent> list           = adapter.GetTorrentList();
		Long          maxUploadToday = Long.MIN_VALUE;
		Torrent       bestToday      = null;

		for (Torrent t : list)
		{
			// Determine best today.
			Long forToday = repo.uploadedWeekly(t);
			if (forToday > maxUploadToday)
			{
				maxUploadToday = forToday;
				bestToday      = t;
			}
		}
		bestToday.setWeeklyUpload(maxUploadToday);
		return bestToday;
	}
}
