/**
 * Code is based on this work: 
 * http://code.google.com/p/transdroid/source/browse/lib/src/org/transdroid/daemon/Transmission/TransmissionAdapter.java?r=8bf5ebe4dd8ddf8241b1a4621027914984287bc2
 */

package transmission;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import settings.Settings;
import transmission.response.POJOs.statistics.TransmissionStatistics;
import transmission.response.POJOs.torrentlist.Torrent;
import transmission.response.POJOs.torrentlist.TorrentList;
import utils.Print;

import java.io.*;
import java.util.List;

public class TransmissionAdapter {
	
	private Settings settings;
	
	// Variables for making the requests
	private HttpClient                  httpclient;
	private HttpPost                    postRequest;
	private CredentialsProvider         credProv;
	private UsernamePasswordCredentials creds;
	
	private String                      sessionHeader = "X-Transmission-Session-Id";
	private String                      sessionToken;

	public TransmissionAdapter(Settings settings)
	{
		this.settings = settings;
		
		InitHttpClient();
	}
	//-------------------------------------------------------------------------/
	//---- INITIALISATION -----------------------------------------------------/
	//-------------------------------------------------------------------------/
	private void InitHttpClient()
	{
		// Setup credentials
		credProv = new BasicCredentialsProvider();
		creds    = new UsernamePasswordCredentials(settings.getUsername(), 
				                                   settings.getPassword());
		credProv.setCredentials(AuthScope.ANY, creds);
		
		httpclient = HttpClientBuilder.create()
				                      .setDefaultCredentialsProvider(credProv)
				                      .build();
	}
	//-------------------------------------------------------------------------/
	//---- GENERAL REQUEST HANDLER --------------------------------------------/
	//-------------------------------------------------------------------------/
	private JsonObject MakeRequest(JsonObject data) throws ClientProtocolException, IOException
	{
		// Create a new request with the URL from settings.
		postRequest = new HttpPost(settings.getRpcUrl());
		
		// Add data request.
		String dataString = new Gson().toJson(data);
		postRequest.setEntity(new StringEntity(dataString, "UTF-8"));
		
		// Attach the stored token, if we have any.
		if(sessionToken != null)
			postRequest.addHeader(sessionHeader, sessionToken);
		
		// Execute the request.
		HttpResponse response = httpclient.execute(postRequest);
		
		
		// Handle possible wrong return values.
        // Authentication error?
        if (response.getStatusLine().getStatusCode() == 401)
                Print.message(this.getClass(), "401 HTTP response (username or password incorrect)");
        
        // 409 error because of a session id?
        if (response.getStatusLine().getStatusCode() == 409) {
                
                // Retry post, but this time with the new session token that was encapsulated in the 409 response.
                sessionToken = response.getFirstHeader(sessionHeader).getValue();
                postRequest.addHeader(sessionHeader, sessionToken);
                response = httpclient.execute(postRequest);
        }
        
		if (response.getStatusLine().getStatusCode() == 200) {
			// Read out the response and return as Gson.
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				// Read JSON response.
				java.io.InputStream instream = entity.getContent();
				String result = ConvertStreamToString(instream);

				// Return a JSON object.
				return new Gson().fromJson(result, JsonElement.class)
						.getAsJsonObject();
			}
		}
		return null;
	}
	//-------------------------------------------------------------------------/
	//---- TRANSMISSION STATISTICS --------------------------------------------/
	//-------------------------------------------------------------------------/
	public TransmissionStatistics GetStatistics() throws ClientProtocolException, IOException
	{
		String requestString = "{\"method\":\"session-stats\"}";
		
		// Build the JSON object.
		JsonParser parser = new JsonParser();
	    JsonObject requestObject = (JsonObject)parser.parse(requestString);
	    
	    // Make request and return.
		JsonObject result =  MakeRequest(requestObject);
		return new Gson().fromJson(result.toString(), TransmissionStatistics.class);
			
	}
	//-------------------------------------------------------------------------/
	//---- TRANSMISSION TORRENT MUTATORS --------------------------------------/
	//-------------------------------------------------------------------------/
	public boolean deleteTorrent(Torrent t) throws ClientProtocolException, IOException
	{
        Print.message(this.getClass(), "Deleting torrent: "+ t.getName());
		String requestString = "{\r\n" + 
				"   \"arguments\":{\r\n" + 
				"      \"ids\":[\r\n" + 
				"         " + t.getId() + "\r\n" + 
				"      ],\r\n" + 
				"      \"delete-locla-data\":\"true\"\r\n" + 
				"   },\r\n" + 
				"   \"method\":\"torrent-remove\"\r\n" + 
				"}";
		// Build the JSON object.
		JsonParser parser = new JsonParser();
	    JsonObject requestObject = (JsonObject)parser.parse(requestString);
	    
	    // Make request and return.
		JsonObject result =  MakeRequest(requestObject);
		System.out.println(result);
		return true;
	}
	//-------------------------------------------------------------------------/
	//---- TRANSMISSION TORRENTLIST -------------------------------------------/
	//-------------------------------------------------------------------------/
	/**
	 * This method gets the entire list of torrents running in transmission.
	 * The method only returns a list of named and ID's. To update these values 
	 * the details will have to be requested for each torrent seperatly.
	 * @return Returns TorrentList, a list of torrents with name and ID.
	 */
	public List<Torrent> GetTorrentList() throws ClientProtocolException, IOException
	{
		String requestString = "{ \"arguments\": { \"fields\": [ \"id\", \"name\", \"magnetLink\"] }, \"method\": \"torrent-get\" }";
		
		// Build the JSON object.
		JsonParser parser = new JsonParser();
	    JsonObject requestObject = (JsonObject)parser.parse(requestString);
	    
	    // Make request and return.
		JsonObject result =  MakeRequest(requestObject);
		TorrentList r =  new Gson().fromJson(result.toString(), TorrentList.class);
		return r.getArguments().getTorrents();
	}
	//-------------------------------------------------------------------------/
	//---- TRANSMISSION TORRENT DETAILS ---------------------------------------/
	//-------------------------------------------------------------------------/
	public void UpdateTorrentDetails(Torrent torrent) throws ClientProtocolException, IOException 
	{
		String requestString = "{\n" + 
				"   \"arguments\":{\n" + 
				"      \"fields\":[\n" + 
				"         \"activityDate\",\n" + 
				"         \"addedDate\",\n" + 
				"         \"bandwidthPriority\",\n" + 
				"         \"comment\",\n" + 
				"         \"corruptEver\",\n" + 
				"         \"creator\",\n" + 
				"         \"dateCreated\",\n" + 
				"         \"desiredAvailable\",\n" + 
				"         \"doneDate\",\n" + 
				"         \"downloadDir\",\n" + 
				"         \"downloadedEver\",\n" + 
				"         \"downloadLimit\",\n" + 
				"         \"downloadLimited\",\n" + 
				"         \"error\",\n" + 
				"         \"errorString\",\n" + 
				"         \"eta\",\n" + 
				"         \"etaIdle\",\n" + 
				"         \"files\",\n" + 
				"         \"fileStats\",\n" + 
				"         \"hashString\",\n" + 
				"         \"haveUnchecked\",\n" + 
				"         \"haveValid\",\n" + 
				"         \"honorsSessionLimits\",\n" + 
				"         \"id\",\n" + 
				"         \"isFinished\",\n" + 
				"         \"isPrivate\",\n" + 
				"         \"isStalled\",\n" + 
				"         \"leftUntilDone\",\n" + 
				"         \"magnetLink\",\n" + 
				"         \"manualAnnounceTime\",\n" + 
				"         \"maxConnectedPeers\",\n" + 
				"         \"metadataPercentComplete\",\n" + 
				"         \"name\",\n" + 
				"         \"peer-limit\",\n" + 
				"         \"peers\",\n" + 
				"         \"peersConnected\",\n" + 
				"         \"peersFrom\",\n" + 
				"         \"peersGettingFromUs\",\n" + 
				"         \"peersSendingToUs\",\n" + 
				"         \"percentDone\",\n" + 
				"         \"pieces\",\n" + 
				"         \"pieceCount\",\n" + 
				"         \"pieceSize\",\n" + 
				"         \"priorities\",\n" + 
				"         \"queuePosition\",\n" + 
				"         \"rateDownload (B/s\",\n" + 
				"         \"rateUpload (B/s\",\n" + 
				"         \"recheckProgress\",\n" + 
				"         \"secondsDownloading\",\n" + 
				"         \"secondsSeeding\",\n" + 
				"         \"seedIdleLimit\",\n" + 
				"         \"seedIdleMode\",\n" + 
				"         \"seedRatioLimit\",\n" + 
				"         \"seedRatioMode\",\n" + 
				"         \"sizeWhenDone\",\n" + 
				"         \"startDate\",\n" + 
				"         \"status\",\n" + 
				"         \"trackers\",\n" + 
				"         \"trackerStats\",\n" + 
				"         \"totalSize\",\n" + 
				"         \"torrentFile\",\n" + 
				"         \"uploadedEver\",\n" + 
				"         \"uploadLimit\",\n" + 
				"         \"uploadLimited\",\n" + 
				"         \"uploadRatio\",\n" + 
				"         \"wanted\",\n" + 
				"         \"webseeds\",\n" + 
				"         \"webseedsSendingToUs\",\n" + 
				"         \"peers\"\n" +
				"      ],\n" + 
				"      \"ids\":[\n" + 
				"         "+ torrent.getId() + "\n" + 
				"      ]\n" + 
				"   },\n" + 
				"   \"method\":\"torrent-get\"\n" + 
				"}";
		
		// Build the JSON object.
		JsonParser parser = new JsonParser();
	    JsonObject requestObject = (JsonObject)parser.parse(requestString);
	    
	    // Make request and return.
		JsonObject result =  MakeRequest(requestObject);
		TorrentList torrentList =  new Gson().fromJson(result.toString(), TorrentList.class);
		
		// Check to see if the list contains a torrent.
		Torrent detailedTorrent = null;
		if (torrentList.getArguments().getTorrents().size() > 0)
			for (Torrent t : torrentList.getArguments().getTorrents())
				if (t.getId().equals(torrent.getId()))
					detailedTorrent = t;
		
		// Update the details in the object in the paramter.
		if(detailedTorrent != null)
		{
			torrent.UpdateDetails(detailedTorrent);
		}
	}
	//-------------------------------------------------------------------------/
	//---- HELPER FUNCTIONS ---------------------------------------------------/
	//-------------------------------------------------------------------------/
    /*
     * To convert the InputStream to String we use the BufferedReader.readLine()
     * method. We iterate until the BufferedReader return null which means
     * there's no more data to read. Each line will appended to a StringBuilder
     * and returned as String.
     * 
     * Taken from http://senior.ceng.metu.edu.tr/2009/praeda/2009/01/11/a-simple-restful-client-at-android/
     */
    private static String ConvertStreamToString(InputStream is, String encoding) 
    		throws UnsupportedEncodingException {
        InputStreamReader isr;
        if (encoding != null) {
                isr = new InputStreamReader(is, encoding);
        } else {
                isr = new InputStreamReader(is);
        }
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    private static String ConvertStreamToString(InputStream is) {
        try {
                        return ConvertStreamToString(is, null);
                } catch (UnsupportedEncodingException e) {
                        // Since this is going to use the default encoding, 
                	    // it is never going to crash on an UnsupportedEncodingException
                        e.printStackTrace();
                        return null;
                }
    }
}
