/**
 * This class encapsulates the response from Transmission that contains all the data-traffic statistics.
 * It makes use of CumulativeStats.java, CurrentStats.java and Arguments.java.
 */
package transmission.response.POJOs.statistics;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class TransmissionStatistics {

}
