
package transmission.response.POJOs.statistics;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Arguments {

    @Expose
    private Integer activeTorrentCount;
    @SerializedName("cumulative-stats")
    @Expose
    private CumulativeStats cumulativeStats;
    @SerializedName("current-stats")
    @Expose
    private CurrentStats currentStats;
    @Expose
    private Integer downloadSpeed;
    @Expose
    private Integer pausedTorrentCount;
    @Expose
    private Integer torrentCount;
    @Expose
    private Integer uploadSpeed;

    public Integer getActiveTorrentCount() {
        return activeTorrentCount;
    }

    public void setActiveTorrentCount(Integer activeTorrentCount) {
        this.activeTorrentCount = activeTorrentCount;
    }

    public CumulativeStats getCumulativeStats() {
        return cumulativeStats;
    }

    public void setCumulativeStats(CumulativeStats cumulativeStats) {
        this.cumulativeStats = cumulativeStats;
    }

    public CurrentStats getCurrentStats() {
        return currentStats;
    }

    public void setCurrentStats(CurrentStats currentStats) {
        this.currentStats = currentStats;
    }

    public Integer getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(Integer downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public Integer getPausedTorrentCount() {
        return pausedTorrentCount;
    }

    public void setPausedTorrentCount(Integer pausedTorrentCount) {
        this.pausedTorrentCount = pausedTorrentCount;
    }

    public Integer getTorrentCount() {
        return torrentCount;
    }

    public void setTorrentCount(Integer torrentCount) {
        this.torrentCount = torrentCount;
    }

    public Integer getUploadSpeed() {
        return uploadSpeed;
    }

    public void setUploadSpeed(Integer uploadSpeed) {
        this.uploadSpeed = uploadSpeed;
    }

}
