
package transmission.response.POJOs.statistics;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CurrentStats {

    @Expose
    private Long downloadedBytes;
    @Expose
    private Long filesAdded;
    @Expose
    private Long secondsActive;
    @Expose
    private Long sessionCount;
    @Expose
    private Long uploadedBytes;

    public Long getDownloadedBytes() {
        return downloadedBytes;
    }

    public void setDownloadedBytes(Long downloadedBytes) {
        this.downloadedBytes = downloadedBytes;
    }

    public Long getFilesAdded() {
        return filesAdded;
    }

    public void setFilesAdded(Long filesAdded) {
        this.filesAdded = filesAdded;
    }

    public Long getSecondsActive() {
        return secondsActive;
    }

    public void setSecondsActive(Long secondsActive) {
        this.secondsActive = secondsActive;
    }

    public Long getSessionCount() {
        return sessionCount;
    }

    public void setSessionCount(Long sessionCount) {
        this.sessionCount = sessionCount;
    }

    public Long getUploadedBytes() {
        return uploadedBytes;
    }

    public void setUploadedBytes(Long uploadedBytes) {
        this.uploadedBytes = uploadedBytes;
    }

}
