
package transmission.response.POJOs.torrentlist;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PeersFrom {

    @Expose
    private Long fromCache;
    @Expose
    private Long fromDht;
    @Expose
    private Long fromIncoming;
    @Expose
    private Long fromLpd;
    @Expose
    private Long fromLtep;
    @Expose
    private Long fromPex;
    @Expose
    private Long fromTracker;

    public Long getFromCache() {
        return fromCache;
    }

    public void setFromCache(Long fromCache) {
        this.fromCache = fromCache;
    }

    public Long getFromDht() {
        return fromDht;
    }

    public void setFromDht(Long fromDht) {
        this.fromDht = fromDht;
    }

    public Long getFromIncoming() {
        return fromIncoming;
    }

    public void setFromIncoming(Long fromIncoming) {
        this.fromIncoming = fromIncoming;
    }

    public Long getFromLpd() {
        return fromLpd;
    }

    public void setFromLpd(Long fromLpd) {
        this.fromLpd = fromLpd;
    }

    public Long getFromLtep() {
        return fromLtep;
    }

    public void setFromLtep(Long fromLtep) {
        this.fromLtep = fromLtep;
    }

    public Long getFromPex() {
        return fromPex;
    }

    public void setFromPex(Long fromPex) {
        this.fromPex = fromPex;
    }

    public Long getFromTracker() {
        return fromTracker;
    }

    public void setFromTracker(Long fromTracker) {
        this.fromTracker = fromTracker;
    }

}
