
package transmission.response.POJOs.torrentlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;
import org.joda.time.Days;
import utils.Print;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Torrent {

	@Expose
	private Long              activityDate;
	@Expose
	private Long              addedDate;
	@Expose
	private Long              bandwidthPriority;
	@Expose
	private String            comment;
	@Expose
	private Long              corruptEver;
	@Expose
	private String            creator;
	@Expose
	private Long              dateCreated;
	@Expose
	private Long              desiredAvailable;
	@Expose
	private Long              doneDate;
	@Expose
	private String            downloadDir;
	@Expose
	private Long              downloadLimit;
	@Expose
	private Boolean           downloadLimited;
	@Expose
	private Long              downloadedEver;
	@Expose
	private Long              error;
	@Expose
	private String            errorString;
	@Expose
	private Long              eta;
	@Expose
	private List<FileStat>    fileStats                = new ArrayList<FileStat>();
	@Expose
	private List<File>        files                    = new ArrayList<File>();
	@Expose
	private String            hashString;
	@Expose
	private Long              haveUnchecked;
	@Expose
	private Long              haveValid;
	@Expose
	private Boolean           honorsSessionLimits;
	@Expose
	private Long              id;
	@Expose
	private Boolean           isFinished;
	@Expose
	private Boolean           isPrivate;
	@Expose
	private Boolean           isStalled;
	@Expose
	private Long              leftUntilDone;
	@Expose
	private String            magnetLink;
	@Expose
	private Long              manualAnnounceTime;
	@Expose
	private Long              maxConnectedPeers;
	@Expose
	private Long              metadataPercentComplete;
	@Expose
	private String            name;
	@SerializedName("peer-limit")
	@Expose
	private Long              peerLimit;
	@Expose
	private List<Peer>        peers                    = new ArrayList<Peer>();
	@Expose
	private Long              peersConnected;
	@Expose
	private PeersFrom         peersFrom;
	@Expose
	private Long              peersGettingFromUs;
	@Expose
	private Long              peersSendingToUs;
	@Expose
	private Double            percentDone;
	@Expose
	private Long              pieceCount;
	@Expose
	private Long              pieceSize;
	@Expose
	private String            pieces;
	@Expose
	private List<Long>        priorities               = new ArrayList<Long>();
	@Expose
	private Long              queuePosition;
	@Expose
	private Long              recheckProgress;
	@Expose
	private Long              secondsDownloading;
	@Expose
	private Long              secondsSeeding;
	@Expose
	private Long              seedIdleLimit;
	@Expose
	private Long              seedIdleMode;
	@Expose
	private Long              seedRatioLimit;
	@Expose
	private Long              seedRatioMode;
	@Expose
	private Long              sizeWhenDone;
	@Expose
	private Long              startDate;
	@Expose
	private Long              status;
	@Expose
	private String            torrentFile;
	@Expose
	private Long              totalSize;
	@Expose
	private List<TrackerStat> trackerStats             = new ArrayList<TrackerStat>();
	@Expose
	private List<Tracker>     trackers                 = new ArrayList<Tracker>();
	@Expose
	private Long              uploadLimit;
	@Expose
	private Boolean           uploadLimited;
	@Expose
	private Float             uploadRatio;
	@Expose
	private Long              uploadedEver;
	@Expose
	private List<Long>        wanted                   = new ArrayList<Long>();
	@Expose
	private List<Object>      webseeds                 = new ArrayList<Object>();
	@Expose
	private Long              webseedsSendingToUs;
	
	private Long dailyUpload;
	
	private Long weeklyUpload;


	//-------------------------------------------------------------------------/
    //---- LOGIC FUNCTIONS ----------------------------------------------------/
    //-------------------------------------------------------------------------/
    /**
     * Method calculates the age in number of days for this torrent.
     */
    public int getAgeInDays()
    {
        if(this.dateCreated == null)
        {
            Print.error(this.getClass(), "Date null. Fetch details first!");
            throw new Error("Details not fetched for torrent");
        }
        else
            	return Days.daysBetween(new DateTime(this.dateCreated * 1000).toLocalDate(), new DateTime().toLocalDate()).getDays();
    }
    
    /**
     * This method takes a torrent file and copies in the detail fields.
     * These are actually all fields. 
     *
     * @param torrentDetails : A torrent instance from which we will copy all field values.
     */
    public void UpdateDetails(Torrent torrentDetails)
    {
    	activityDate            = torrentDetails.getActivityDate();
    	addedDate               = torrentDetails.getAddedDate();
    	bandwidthPriority       = torrentDetails.getBandwidthPriority();
    	comment                 = torrentDetails.getComment();
    	corruptEver             = torrentDetails.getCorruptEver();
    	creator                 = torrentDetails.getCreator();
    	dateCreated             = torrentDetails.getDateCreated();
    	desiredAvailable        = torrentDetails.getDesiredAvailable();
    	doneDate                = torrentDetails.getDoneDate();
    	downloadDir             = torrentDetails.getDownloadDir();
    	downloadLimit           = torrentDetails.getDownloadLimit();
    	downloadLimited         = torrentDetails.getDownloadLimited();
    	downloadedEver          = torrentDetails.getBytesDownloadedEver();
    	error                   = torrentDetails.getError();
    	errorString             = torrentDetails.getErrorString();
    	eta                     = torrentDetails.getEta();
    	fileStats               = torrentDetails.getFileStats();
    	files                   = torrentDetails.getFiles();
    	hashString              = torrentDetails.getHashString();
    	haveUnchecked           = torrentDetails.getHaveUnchecked();
    	haveValid               = torrentDetails.getHaveValid();
    	honorsSessionLimits     = torrentDetails.getHonorsSessionLimits();
    	id                      = torrentDetails.getId();
    	isFinished              = torrentDetails.getIsFinished();
    	isPrivate               = torrentDetails.getIsPrivate();
    	isStalled               = torrentDetails.getIsPrivate();
    	leftUntilDone           = torrentDetails.getLeftUntilDone();
    	magnetLink              = torrentDetails.getMagnetLink();
    	manualAnnounceTime      = torrentDetails.getManualAnnounceTime();
    	maxConnectedPeers       = torrentDetails.getMaxConnectedPeers();
    	metadataPercentComplete = torrentDetails.getMetadataPercentComplete();
    	name                    = torrentDetails.getName();
    	peerLimit               = torrentDetails.getPeerLimit();
    	peers                   = torrentDetails.getPeers();
    	peersConnected          = torrentDetails.getPeersConnected();
    	peersFrom               = torrentDetails.getPeersFrom();
    	peersGettingFromUs      = torrentDetails.getPeersGettingFromUs();
    	peersSendingToUs        = torrentDetails.getPeersSendingToUs();
    	percentDone             = torrentDetails.getPercentDone();
    	pieceCount              = torrentDetails.getPieceCount();
    	pieceSize               = torrentDetails.getPieceSize();
    	pieces                  = torrentDetails.getPieces();
    	priorities              = torrentDetails.getPriorities();
    	queuePosition           = torrentDetails.getQueuePosition();
    	recheckProgress         = torrentDetails.getRecheckProgress();
    	secondsDownloading      = torrentDetails.getSecondsDownloading();
    	secondsSeeding          = torrentDetails.getSecondsSeeding();
    	seedIdleLimit           = torrentDetails.getSeedIdleLimit();
    	seedIdleMode            = torrentDetails.getSeedIdleMode();
    	seedRatioLimit          = torrentDetails.getSeedRatioLimit();
    	seedRatioMode           = torrentDetails.getSeedRatioMode();
    	sizeWhenDone            = torrentDetails.getSizeWhenDone();
    	startDate               = torrentDetails.getStartDate();
    	status                  = torrentDetails.getStatus();
    	torrentFile             = torrentDetails.getTorrentFile();
    	totalSize               = torrentDetails.getTotalSize();
    	trackerStats            = torrentDetails.getTrackerStats();
    	trackers                = torrentDetails.getTrackers();
    	uploadLimit             = torrentDetails.getUploadLimit();
    	uploadLimited           = torrentDetails.getUploadLimited();
    	uploadRatio             = torrentDetails.getUploadRatio();
    	uploadedEver            = torrentDetails.getBytesUploadedEver();
    	wanted                  = torrentDetails.getWanted();
    	webseeds                = torrentDetails.getWebseeds();
    	webseedsSendingToUs     = torrentDetails.getWebseedsSendingToUs();
    }
    //-------------------------------------------------------------------------/
    //---- OVERRIDDEN METHODS -------------------------------------------------/
    //-------------------------------------------------------------------------/
    @Override
    public String toString()
    {
    	return this.name;
    }
    /**
     * Torrent comparison is based on a few basic things:
     * 1) Date created (this can change if we re-add the torrent though)
     * 2) Magnet link: I'm assuming this does not change, even when we re-add the torrent.
     *                 So we might want to pick this one alone as our unique identifier
     *                 for a single torrent.
     */
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((magnetLink == null) ? 0 : magnetLink.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Torrent other = (Torrent) obj;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (magnetLink == null) {
			if (other.magnetLink != null)
				return false;
		} else if (!magnetLink.equals(other.magnetLink))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
    //-------------------------------------------------------------------------/
    //---- GETTERS AND SETTERS ------------------------------------------------/
    //-------------------------------------------------------------------------/
    public Long getActivityDate() {
        return activityDate;
    }

	public void setActivityDate(Long activityDate) {
        this.activityDate = activityDate;
    }

    public Long getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Long addedDate) {
        this.addedDate = addedDate;
    }

    public Long getBandwidthPriority() {
        return bandwidthPriority;
    }

    public void setBandwidthPriority(Long bandwidthPriority) {
        this.bandwidthPriority = bandwidthPriority;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getCorruptEver() {
        return corruptEver;
    }

    public void setCorruptEver(Long corruptEver) {
        this.corruptEver = corruptEver;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDesiredAvailable() {
        return desiredAvailable;
    }

    public void setDesiredAvailable(Long desiredAvailable) {
        this.desiredAvailable = desiredAvailable;
    }

    public Long getDoneDate() {
        return doneDate;
    }

    public void setDoneDate(Long doneDate) {
        this.doneDate = doneDate;
    }

    public String getDownloadDir() {
        return downloadDir;
    }

    public void setDownloadDir(String downloadDir) {
        this.downloadDir = downloadDir;
    }

    public Long getDownloadLimit() {
        return downloadLimit;
    }

    public void setDownloadLimit(Long downloadLimit) {
        this.downloadLimit = downloadLimit;
    }

    public Boolean getDownloadLimited() {
        return downloadLimited;
    }

    public void setDownloadLimited(Boolean downloadLimited) {
        this.downloadLimited = downloadLimited;
    }

    public Long getBytesDownloadedEver() {
        return downloadedEver;
    }

    public void setDownloadedEver(Long downloadedEver) {
        this.downloadedEver = downloadedEver;
    }

    public Long getError() {
        return error;
    }

    public void setError(Long error) {
        this.error = error;
    }

    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }

    public Long getEta() {
        return eta;
    }

    public void setEta(Long eta) {
        this.eta = eta;
    }

    public List<FileStat> getFileStats() {
        return fileStats;
    }

    public void setFileStats(List<FileStat> fileStats) {
        this.fileStats = fileStats;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public String getHashString() {
        return hashString;
    }

    public void setHashString(String hashString) {
        this.hashString = hashString;
    }

    public Long getHaveUnchecked() {
        return haveUnchecked;
    }

    public void setHaveUnchecked(Long haveUnchecked) {
        this.haveUnchecked = haveUnchecked;
    }

    public Long getHaveValid() {
        return haveValid;
    }

    public void setHaveValid(Long haveValid) {
        this.haveValid = haveValid;
    }

    public Boolean getHonorsSessionLimits() {
        return honorsSessionLimits;
    }

    public void setHonorsSessionLimits(Boolean honorsSessionLimits) {
        this.honorsSessionLimits = honorsSessionLimits;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(Boolean isFinished) {
        this.isFinished = isFinished;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Boolean getIsStalled() {
        return isStalled;
    }

    public void setIsStalled(Boolean isStalled) {
        this.isStalled = isStalled;
    }

    public Long getLeftUntilDone() {
        return leftUntilDone;
    }

    public void setLeftUntilDone(Long leftUntilDone) {
        this.leftUntilDone = leftUntilDone;
    }

    public String getMagnetLink() {
        return magnetLink;
    }

    public void setMagnetLink(String magnetLink) {
        this.magnetLink = magnetLink;
    }

    public Long getManualAnnounceTime() {
        return manualAnnounceTime;
    }

    public void setManualAnnounceTime(Long manualAnnounceTime) {
        this.manualAnnounceTime = manualAnnounceTime;
    }

    public Long getMaxConnectedPeers() {
        return maxConnectedPeers;
    }

    public void setMaxConnectedPeers(Long maxConnectedPeers) {
        this.maxConnectedPeers = maxConnectedPeers;
    }

    public Long getMetadataPercentComplete() {
        return metadataPercentComplete;
    }

    public void setMetadataPercentComplete(Long metadataPercentComplete) {
        this.metadataPercentComplete = metadataPercentComplete;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPeerLimit() {
        return peerLimit;
    }

    public void setPeerLimit(Long peerLimit) {
        this.peerLimit = peerLimit;
    }

    public List<Peer> getPeers() {
        return peers;
    }

    public void setPeers(List<Peer> peers) {
        this.peers = peers;
    }

    public Long getPeersConnected() {
        return peersConnected;
    }

    public void setPeersConnected(Long peersConnected) {
        this.peersConnected = peersConnected;
    }

    public PeersFrom getPeersFrom() {
        return peersFrom;
    }

    public void setPeersFrom(PeersFrom peersFrom) {
        this.peersFrom = peersFrom;
    }

    public Long getPeersGettingFromUs() {
        return peersGettingFromUs;
    }

    public void setPeersGettingFromUs(Long peersGettingFromUs) {
        this.peersGettingFromUs = peersGettingFromUs;
    }

    public Long getPeersSendingToUs() {
        return peersSendingToUs;
    }

    public void setPeersSendingToUs(Long peersSendingToUs) {
        this.peersSendingToUs = peersSendingToUs;
    }

    public Double getPercentDone() {
        return percentDone;
    }

    public void setPercentDone(Double percentDone) {
        this.percentDone = percentDone;
    }

    public Long getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(Long pieceCount) {
        this.pieceCount = pieceCount;
    }

    public Long getPieceSize() {
        return pieceSize;
    }

    public void setPieceSize(Long pieceSize) {
        this.pieceSize = pieceSize;
    }

    public String getPieces() {
        return pieces;
    }

    public void setPieces(String pieces) {
        this.pieces = pieces;
    }

    public List<Long> getPriorities() {
        return priorities;
    }

    public void setPriorities(List<Long> priorities) {
        this.priorities = priorities;
    }

    public Long getQueuePosition() {
        return queuePosition;
    }

    public void setQueuePosition(Long queuePosition) {
        this.queuePosition = queuePosition;
    }

    public Long getRecheckProgress() {
        return recheckProgress;
    }

    public void setRecheckProgress(Long recheckProgress) {
        this.recheckProgress = recheckProgress;
    }

    public Long getSecondsDownloading() {
        return secondsDownloading;
    }

    public void setSecondsDownloading(Long secondsDownloading) {
        this.secondsDownloading = secondsDownloading;
    }

    public Long getSecondsSeeding() {
        return secondsSeeding;
    }

    public void setSecondsSeeding(Long secondsSeeding) {
        this.secondsSeeding = secondsSeeding;
    }

    public Long getSeedIdleLimit() {
        return seedIdleLimit;
    }

    public void setSeedIdleLimit(Long seedIdleLimit) {
        this.seedIdleLimit = seedIdleLimit;
    }

    public Long getSeedIdleMode() {
        return seedIdleMode;
    }

    public void setSeedIdleMode(Long seedIdleMode) {
        this.seedIdleMode = seedIdleMode;
    }

    public Long getSeedRatioLimit() {
        return seedRatioLimit;
    }

    public void setSeedRatioLimit(Long seedRatioLimit) {
        this.seedRatioLimit = seedRatioLimit;
    }

    public Long getSeedRatioMode() {
        return seedRatioMode;
    }

    public void setSeedRatioMode(Long seedRatioMode) {
        this.seedRatioMode = seedRatioMode;
    }

    public Long getSizeWhenDone() {
        return sizeWhenDone;
    }

    public void setSizeWhenDone(Long sizeWhenDone) {
        this.sizeWhenDone = sizeWhenDone;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTorrentFile() {
        return torrentFile;
    }

    public void setTorrentFile(String torrentFile) {
        this.torrentFile = torrentFile;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public List<TrackerStat> getTrackerStats() {
        return trackerStats;
    }

    public void setTrackerStats(List<TrackerStat> trackerStats) {
        this.trackerStats = trackerStats;
    }

    public List<Tracker> getTrackers() {
        return trackers;
    }

    public void setTrackers(List<Tracker> trackers) {
        this.trackers = trackers;
    }

    public Long getUploadLimit() {
        return uploadLimit;
    }

    public void setUploadLimit(Long uploadLimit) {
        this.uploadLimit = uploadLimit;
    }

    public Boolean getUploadLimited() {
        return uploadLimited;
    }

    public void setUploadLimited(Boolean uploadLimited) {
        this.uploadLimited = uploadLimited;
    }

    public Float getUploadRatio() {
        return uploadRatio;
    }

    public void setUploadRatio(Float uploadRatio) {
        this.uploadRatio = uploadRatio;
    }

    public Long getBytesUploadedEver() {
        return uploadedEver;
    }

    public void setUploadedEver(Long uploadedEver) {
        this.uploadedEver = uploadedEver;
    }

    public List<Long> getWanted() {
        return wanted;
    }

    public void setWanted(List<Long> wanted) {
        this.wanted = wanted;
    }

    public List<Object> getWebseeds() {
        return webseeds;
    }

    public void setWebseeds(List<Object> webseeds) {
        this.webseeds = webseeds;
    }

    public Long getWebseedsSendingToUs() {
        return webseedsSendingToUs;
    }

    public void setWebseedsSendingToUs(Long webseedsSendingToUs) {
        this.webseedsSendingToUs = webseedsSendingToUs;
    }

    public Long getDailyUpload()
	{
		return dailyUpload;
	}

	public void setDailyUpload(Long weeklyUpload)
	{
		this.dailyUpload = weeklyUpload;
	}

	public Long getWeeklyUpload()
	{
		return weeklyUpload;
	}

	public void setWeeklyUpload(Long weeklyUpload)
	{
		this.weeklyUpload = weeklyUpload;
	}
	
}
