
package transmission.response.POJOs.torrentlist;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Peer {

    @Expose
    private String address;
    @Expose
    private Boolean clientIsChoked;
    @Expose
    private Boolean clientIsInterested;
    @Expose
    private String clientName;
    @Expose
    private String flagStr;
    @Expose
    private Boolean isDownloadingFrom;
    @Expose
    private Boolean isEncrypted;
    @Expose
    private Boolean isIncoming;
    @Expose
    private Boolean isUTP;
    @Expose
    private Boolean isUploadingTo;
    @Expose
    private Boolean peerIsChoked;
    @Expose
    private Boolean peerIsInterested;
    @Expose
    private Long port;
    @Expose
    private Float progress;
    @Expose
    private Long rateToClient;
    @Expose
    private Long rateToPeer;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getClientIsChoked() {
        return clientIsChoked;
    }

    public void setClientIsChoked(Boolean clientIsChoked) {
        this.clientIsChoked = clientIsChoked;
    }

    public Boolean getClientIsInterested() {
        return clientIsInterested;
    }

    public void setClientIsInterested(Boolean clientIsInterested) {
        this.clientIsInterested = clientIsInterested;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getFlagStr() {
        return flagStr;
    }

    public void setFlagStr(String flagStr) {
        this.flagStr = flagStr;
    }

    public Boolean getIsDownloadingFrom() {
        return isDownloadingFrom;
    }

    public void setIsDownloadingFrom(Boolean isDownloadingFrom) {
        this.isDownloadingFrom = isDownloadingFrom;
    }

    public Boolean getIsEncrypted() {
        return isEncrypted;
    }

    public void setIsEncrypted(Boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public Boolean getIsIncoming() {
        return isIncoming;
    }

    public void setIsIncoming(Boolean isIncoming) {
        this.isIncoming = isIncoming;
    }

    public Boolean getIsUTP() {
        return isUTP;
    }

    public void setIsUTP(Boolean isUTP) {
        this.isUTP = isUTP;
    }

    public Boolean getIsUploadingTo() {
        return isUploadingTo;
    }

    public void setIsUploadingTo(Boolean isUploadingTo) {
        this.isUploadingTo = isUploadingTo;
    }

    public Boolean getPeerIsChoked() {
        return peerIsChoked;
    }

    public void setPeerIsChoked(Boolean peerIsChoked) {
        this.peerIsChoked = peerIsChoked;
    }

    public Boolean getPeerIsInterested() {
        return peerIsInterested;
    }

    public void setPeerIsInterested(Boolean peerIsInterested) {
        this.peerIsInterested = peerIsInterested;
    }

    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public Float getProgress() {
        return progress;
    }

    public void setProgress(Float progress) {
        this.progress = progress;
    }

    public Long getRateToClient() {
        return rateToClient;
    }

    public void setRateToClient(Long rateToClient) {
        this.rateToClient = rateToClient;
    }

    public Long getRateToPeer() {
        return rateToPeer;
    }

    public void setRateToPeer(Long rateToPeer) {
        this.rateToPeer = rateToPeer;
    }

}
