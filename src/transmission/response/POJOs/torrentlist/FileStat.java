
package transmission.response.POJOs.torrentlist;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FileStat {

    @Expose
    private Long bytesCompleted;
    @Expose
    private Long priority;
    @Expose
    private Boolean wanted;

    public Long getBytesCompleted() {
        return bytesCompleted;
    }

    public void setBytesCompleted(Long bytesCompleted) {
        this.bytesCompleted = bytesCompleted;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Boolean getWanted() {
        return wanted;
    }

    public void setWanted(Boolean wanted) {
        this.wanted = wanted;
    }

}
