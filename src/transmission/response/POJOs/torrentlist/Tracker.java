
package transmission.response.POJOs.torrentlist;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Tracker {

    @Expose
    private String announce;
    @Expose
    private Long id;
    @Expose
    private String scrape;
    @Expose
    private Long tier;

    public String getAnnounce() {
        return announce;
    }

    public void setAnnounce(String announce) {
        this.announce = announce;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getScrape() {
        return scrape;
    }

    public void setScrape(String scrape) {
        this.scrape = scrape;
    }

    public Long getTier() {
        return tier;
    }

    public void setTier(Long tier) {
        this.tier = tier;
    }

}
