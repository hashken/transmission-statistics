package settings;

import utils.Print;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class Settings {
	
	private String ip;
	private int    port;
	private String directory;
	private String password;
	private String username;
	private int    updateInterval;
	private int    deleteThreshold;
    private int    deleteAgeThreshold;


    //-------------------------------------------------------------------------/
    //---- LOAD SETTINGS FILE FROM DISK ---------------------------------------/
    //-------------------------------------------------------------------------/

    /**
     * Method that loads the properties file. There is a default file present.
     * @throws IOException
     */
    public void   loadSettings() throws IOException
    {
        Properties  prop         = new Properties();
        String      propFileName = "settings.properties";
        InputStream inputStream  = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }
        prop.load(inputStream);
 
        // get the property values
        username           = prop.getProperty("user");
        password           = prop.getProperty("password");
        port               = Integer.parseInt(prop.getProperty("port"));
        ip                 = prop.getProperty("address");
        directory          = prop.getProperty("directory");
        updateInterval     = Integer.parseInt(prop.getProperty("updateInterval"));
        deleteThreshold    = Integer.parseInt(prop.getProperty("deleteThreshold"));
        deleteAgeThreshold = Integer.parseInt(prop.getProperty("deleteAgeThreshold"));
        
        Print.message(this.getClass(), "Settings loaded!");
	}
	//-------------------------------------------------------------------------/
	//---- LOGIC METHODS ------------------------------------------------------/
	//-------------------------------------------------------------------------/
	/**
	 * This method builds the entire url to contact transmission
	 * based on the properties loaded fro mthe properties file.
	 * @return
	 */
	public String getRpcUrl()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("http://");
		sb.append(this.ip);
		sb.append(":");
		sb.append("" + this.port);
		sb.append("/" + this.directory);
		sb.append("/rpc/");
		//Print.Print(this.getClass(), "Requesting to: " + sb.toString());
		return sb.toString();
	}
	//-------------------------------------------------------------------------/
	//---- GETTERS AND SETTERS ------------------------------------------------/
	//-------------------------------------------------------------------------/
	public String getIp() {
		return ip;
	}
	public int    getPort() {
		return port;
	}
	public String getDirectory() {
		return directory;
	}
	public String getPassword() {
		return password;
	}
	public String getUsername() {
		return username;
	}
	public int    getUpdateInterval()
	{
		return updateInterval;
	}
	public void   setUpdateInterval(int updateInterval)
	{
		this.updateInterval = updateInterval;
	}
	public int    getDeleteThreshold()
	{
		return deleteThreshold;
	}
	public void   setDeleteThreshold(int deleteThreshold)
	{
		this.deleteThreshold = deleteThreshold;
	}
    public int    getDeleteAgeThreshold()
    {
        return deleteAgeThreshold;
    }
    public void   setDeleteAgeThreshold(int deleteAgeThreshold)
    {
        this.deleteAgeThreshold = deleteAgeThreshold;
    }
	
}
