package utils;

import org.joda.time.DateTime;

import java.io.IOException;

public class Print {
	private final static boolean messagesEnabled = true;
	private final static boolean errorsEnabled   = true;
	private final static boolean debugEnabled    = false;

	/**
	 * Prints a regular message with the classname prepended to it.
	 * @param c
	 * @param msg
	 */
	public static void debugMessage(@SuppressWarnings("rawtypes") Class c, String msg)
	{
		if(debugEnabled)
		{
			DateTime d = new DateTime();
			System.out.println(d.toString() + " >> " +  c.getName() + ": " + msg);
		}
	}
	
	/**
	 * Prints an error message with the class name prepended to it.
	 * @param c
	 * @param msg
	 */
	public static void error(@SuppressWarnings("rawtypes") Class c, String msg)
	{
		if (errorsEnabled)
		{
			DateTime d = new DateTime();
			System.err
					.println(d.toString() + " >> " + c.getName() + ": " + msg);
		}
	}

    /**
     * Prints a regular message which should typically always be shown in the console.
     * @param c
     * @param msg
     */
	public static void message(@SuppressWarnings("rawtypes") Class c, String msg)
	{
		if (messagesEnabled)
		{
			DateTime d = new DateTime();
			System.out
					.println(d.toString() + " >> " + c.getName() + ": " + msg);
		}
	}

    public static void clearConsole()
    {
        //for(int i = 0; i < 100; i++) System.out.println("");
        //	    try
        {
            final String os = System.getProperty("os.name");

            try
            {
                if (os.contains("Windows"))
                    Runtime.getRuntime().exec("cls");
                else
                {
                    System.out.print("\033[H\033[2J");
                    System.out.flush();
                }
            } catch (IOException e)
            {
                Print.error(Print.class.getClass(), "Error clearing screen");
            }
        }
    }
}

