package utils;

/**
 * Created by christophe on 24.08.14.
 */
public class StringUtils
{
    public static String truncate(String s, int size)
    {
        if(s.length() < size)
            return s;
        return s.substring(0, size);

    }
}
