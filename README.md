# README #
### What is this repository for? ###

A Java console application used to gather advanced statistics for the Transmission program. As well as remove dead torrents.

### How do I get set up? ###

Import in Eclipse, create a settings file in the resources folder and run it from a jar.

### Contribution guidelines ###

You can always help me finetune this application. Fork it or send me a message on G+ (https://plus.google.com/u/0/+ChristopheDeTroyer).
